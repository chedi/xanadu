package com.xanadu.activities;

import java.util.List     ;
import java.util.ArrayList;

import com.xanadu.data.Poseur            ;
import com.xanadu.data.SimInfo           ;
import com.xanadu.application.R          ;
import com.xanadu.utils.CommonUtils      ;
import com.xanadu.application.Xanadu     ;
import com.xanadu.utils.ActivityLauncher ;
import com.xanadu.utils.SimpleAlertDialog;

import android.util.Log                  ;
import android.os.Bundle                 ;
import android.view.View                 ;
import android.app.Activity              ;
import android.widget.Button             ;
import android.content.Intent            ;
import android.content.Context           ;
import android.net.ConnectivityManager   ;
import android.view.View.OnClickListener ;
import android.telephony.TelephonyManager;

public class ProjectX extends Activity {
	private static int     status   = Xanadu.UNINTIALIZED;
	private static boolean is_loged = false;

	private Button login           ;
	private Button logout          ;
	private Button reset_password  ;
	private Button synchronize_data;

	public static Xanadu           core    ;
	public static Poseur           poseur  ;
	public static Context          context ;
	public static SimInfo          siminfo ;
	public static TelephonyManager manager ;
	public static String           password;

	private ActivityLauncher<SmsActivity        , ProjectX> sms_activity        ;
	private ActivityLauncher<MainActivity       , ProjectX> main_activity       ;
	private ActivityLauncher<EmailActivity      , ProjectX> email_activity      ;
	private ActivityLauncher<LoginActivity      , ProjectX> login_activity      ;
	private ActivityLauncher<SynchronizeActivity, ProjectX> synchronize_activity;

	private static List<ActivityLauncher<SmsActivity        , ProjectX>.Extras<?>> sms_extras             = new ArrayList<ActivityLauncher<SmsActivity        , ProjectX>.Extras<?>>();
	private static List<ActivityLauncher<EmailActivity      , ProjectX>.Extras<?>> email_extras           = new ArrayList<ActivityLauncher<EmailActivity      , ProjectX>.Extras<?>>();
	private static List<ActivityLauncher<SynchronizeActivity, ProjectX>.Extras<?>> online_synchro_extras  = new ArrayList<ActivityLauncher<SynchronizeActivity, ProjectX>.Extras<?>>();
	private static List<ActivityLauncher<SynchronizeActivity, ProjectX>.Extras<?>> offline_synchro_extras = new ArrayList<ActivityLauncher<SynchronizeActivity, ProjectX>.Extras<?>>();

	public void onCreate(Bundle saved_instance_state) {
		super.onCreate(saved_instance_state);
		setContentView(R.layout.welcome);

		context = this;
		core    = (Xanadu) getApplication();
		manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

		sms_activity         = new ActivityLauncher<SmsActivity        , ProjectX>(SmsActivity        .class, Xanadu.SMS_ACTIVITY        , context);
		main_activity        = new ActivityLauncher<MainActivity       , ProjectX>(MainActivity       .class, Xanadu.MAIN_ACTIVITY       , context);
		email_activity       = new ActivityLauncher<EmailActivity      , ProjectX>(EmailActivity      .class, Xanadu.EMAIL_ACTIVITY      , context);
		login_activity       = new ActivityLauncher<LoginActivity      , ProjectX>(LoginActivity      .class, Xanadu.LOGIN_ACTIVITY      , context);
		synchronize_activity = new ActivityLauncher<SynchronizeActivity, ProjectX>(SynchronizeActivity.class, Xanadu.SYNCHRONIZE_ACTIVITY, context);

		online_synchro_extras .add(synchronize_activity.new Extras<Boolean>(Xanadu.OFFLINE_RELOAD, false));
		offline_synchro_extras.add(synchronize_activity.new Extras<Boolean>(Xanadu.OFFLINE_RELOAD, true ));

		if(getSimState()){
			siminfo = new SimInfo(0, manager.getDeviceId(), manager.getLine1Number(), manager.getSimSerialNumber());
		} else {
			status = Xanadu.SIM_MISSING_ERROR;
			finish();
		}

		if (core.getFirstRun()) {
			if (isOnline()) { synchronize_activity.setExtras(online_synchro_extras );}
			else            { synchronize_activity.setExtras(offline_synchro_extras);}
		} else {
			synchronize_activity.setExtras(offline_synchro_extras);
		}

		core.setRunned();
		synchronize_activity.run();

		login            = (Button) findViewById(R.id.login           );
		logout           = (Button) findViewById(R.id.logout          );
		reset_password   = (Button) findViewById(R.id.reset_password  );
		synchronize_data = (Button) findViewById(R.id.synchronize_data);

		login            .setEnabled(!is_loged );
		logout           .setEnabled( is_loged );
		reset_password   .setEnabled( isOnline());
		synchronize_data .setEnabled( isOnline());

		login.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				try                 { login_activity.run();}
				catch (Exception e) { Log.d(getString(R.string.application_name), e.getMessage());
		}}});

		logout.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try {
					is_loged = false;
					login .setEnabled(!is_loged);
					logout.setEnabled( is_loged);
				} catch (Exception e) {
					Log.d(getString(R.string.application_name), e.getMessage());
		}}});

		synchronize_data.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				try {
					if(simPresent() && isOnline()){
						synchronize_activity.setExtras(online_synchro_extras);
						synchronize_activity.run();
					} else {
						new SimpleAlertDialog<Integer>(context, getString(R.string.sim_and_connection_mandatory));
				}} catch (Exception e) {
					Log.d(getString(R.string.application_name), e.getMessage());
		}}});

		reset_password   .setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				try {
					final String reset_password_body = getString(R.string.password_reset_message) + siminfo.getSerialNumber();
					if(isOnline()){
						final String reset_password_subject    = getString(R.string.password_reset_subject);
						final String reset_password_recipients = Xanadu.PASSWORD_RESET_RECIPIENTS;
						email_extras.add(email_activity.new Extras<String>(Xanadu.EMAIL_BODY     , reset_password_body      ));
						email_extras.add(email_activity.new Extras<String>(Xanadu.EMAIL_SUBJECT  , reset_password_subject   ));
						email_extras.add(email_activity.new Extras<String>(Xanadu.EMAIL_RECIPIENT, reset_password_recipients));
						email_activity.setExtras(email_extras);
						email_activity.run();
					} else {
						sms_extras.add(sms_activity.new Extras<String>(Xanadu.SMS_DEVICE_ID, siminfo.getDeviceId    ()));
						sms_extras.add(sms_activity.new Extras<String>(Xanadu.SMS_PHONE_SIM, siminfo.getSerialNumber()));
						sms_extras.add(sms_activity.new Extras<String>(Xanadu.SMS_MESSAGE  , reset_password_body      ));
						sms_activity.setExtras(sms_extras);
						sms_activity.run();
					}
				} catch (Exception e) {
					Log.d(getString(R.string.application_name), e.getStackTrace().toString());
		}}});
	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if(cm.getActiveNetworkInfo() != null){
			return cm.getActiveNetworkInfo().isConnectedOrConnecting();
		}
		return false;
	}

	static public boolean isLoged   () { return is_loged; }
	static public boolean simPresent() { return siminfo != null && siminfo.getSerialNumber() != null ? true : false ; }

	protected boolean getSimState(){
		boolean sim_state = false;
		switch (manager.getSimState()) {
			case TelephonyManager.SIM_STATE_READY         : sim_state = true ; break;
			case TelephonyManager.SIM_STATE_ABSENT        : sim_state = false; break;
			case TelephonyManager.SIM_STATE_UNKNOWN       : sim_state = false; break;
			case TelephonyManager.SIM_STATE_PIN_REQUIRED  : sim_state = false; break;
			case TelephonyManager.SIM_STATE_PUK_REQUIRED  : sim_state = false; break;
			case TelephonyManager.SIM_STATE_NETWORK_LOCKED: sim_state = false; break;
		}
		return sim_state;
	}

	public void finish(){
		switch(status){
			case Xanadu.SIM_MISSING_ERROR    : new SimpleAlertDialog<Integer>(context, getString(R.string.sim_card_not_detected)); break;
			case Xanadu.FORCED_QUIT_STATE    : new SimpleAlertDialog<Integer>(context, getString(R.string.application_unusable )); break;
			case Xanadu.SYNCHRONIZATION_ERROR: new SimpleAlertDialog<Integer>(context, getString(R.string.sync_data_failed     )); break;
		}
		super.finish();
	}

	public void onDestory() {
		super.onDestroy();
		CommonUtils.killApp(true);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode){
			case Xanadu.LOGIN_ACTIVITY:
				if(resultCode == RESULT_OK){
					poseur   = (Poseur) data.getExtras().get(Xanadu.LOGIN_INFO         );
					password = (String) data.getExtras().get(Xanadu.LOGIN_USER_PASSWORD);
					if(poseur == null) {
						new SimpleAlertDialog<Integer>(context, getString(R.string.authentication_failed));
					} else {
						is_loged = true;
						main_activity.run();
					}
				} else {new SimpleAlertDialog<Integer>(context, getString(R.string.authentication_failed));}
				break;
			case Xanadu.SYNCHRONIZE_ACTIVITY:
				if(resultCode == RESULT_OK){ new SimpleAlertDialog<Integer>(context, getString(R.string.sync_data_successful));}
				else                       { new SimpleAlertDialog<Integer>(context, getString(R.string.sync_data_failed    ));}
				break;
			case Xanadu.SMS_ACTIVITY:
				if(resultCode == RESULT_OK){ new SimpleAlertDialog<Integer>(context, getString(R.string.password_reset_success));}
				else                       { new SimpleAlertDialog<Integer>(context, getString(R.string.password_reset_failure));}
				break;
			case Xanadu.EMAIL_ACTIVITY:
				break;
	}}
}