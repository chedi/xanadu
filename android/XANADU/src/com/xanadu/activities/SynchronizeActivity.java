package com.xanadu.activities;

import java.util.List;

import android.util.Log          ;
import android.os.Bundle         ;
import android.app.Activity      ;
import android.content.Intent    ;
import android.content.Context   ;
import android.app.ProgressDialog;

import com.xanadu.data.Type                           ;
import com.xanadu.data.Panel                          ;
import com.xanadu.data.Format                         ;
import com.xanadu.data.Poseur                         ;
import com.xanadu.data.Campaign                       ;
import com.xanadu.application.R                       ;
import com.xanadu.utils.SqlHelper                     ;
import com.xanadu.application.Xanadu                  ;
import com.xanadu.consumers.PanelServiceConsumer      ;
import com.xanadu.consumers.PosterServiceConsumer     ;
import com.xanadu.consumers.CampaignServiceConsumer   ;
import com.xanadu.consumers.PanelTypeServiceConsumer  ;
import com.xanadu.consumers.PanelFormatServiceConsumer;

public class SynchronizeActivity extends Activity {
	private transient Context context;
	private transient boolean sync_ok;
	private transient boolean sync_ko;

	private static List<Panel   > all_panels       ;
	private static List<Poseur  > all_poseurs      ;
	private static List<Campaign> all_compains     ;
	private static List<Type    > all_panel_types  ;
	private static List<Format  > all_panel_formats;

	public void onCreate(final Bundle saved_instance) {
		super.onCreate(saved_instance);
		context = this ;
		sync_ko = false;
		sync_ok = false;
		final boolean        offlineReload   = getIntent().getExtras().getBoolean(Xanadu.OFFLINE_RELOAD);
		final ProgressDialog progress_dialog = ProgressDialog.show(SynchronizeActivity.this, getString(R.string.please_wait), getString(R.string.data_synchronization), true);

		new Thread() {
			public void run() {
				final SqlHelper helper = new SqlHelper(context);
				try {
					PanelServiceConsumer       pn_consumer;
					PosterServiceConsumer      po_consumer;
					CampaignServiceConsumer    cm_consumer;
					PanelTypeServiceConsumer   pt_consumer;
					PanelFormatServiceConsumer pf_consumer;
					if(!offlineReload){
						final String device_id  = ProjectX.siminfo.getDeviceId    ();
						final String sim_serial = ProjectX.siminfo.getSerialNumber();

						pn_consumer = new PanelServiceConsumer      (Xanadu.HOST_SERVER, Xanadu.HOST_PORT, sim_serial, device_id);
						po_consumer = new PosterServiceConsumer     (Xanadu.HOST_SERVER, Xanadu.HOST_PORT, sim_serial, device_id);
						cm_consumer = new CampaignServiceConsumer   (Xanadu.HOST_SERVER, Xanadu.HOST_PORT, sim_serial, device_id);
						pt_consumer = new PanelTypeServiceConsumer  (Xanadu.HOST_SERVER, Xanadu.HOST_PORT, sim_serial, device_id);
						pf_consumer = new PanelFormatServiceConsumer(Xanadu.HOST_SERVER, Xanadu.HOST_PORT, sim_serial, device_id);

						sync_ko =
							!helper.synchronize(pn_consumer) ||
							!helper.synchronize(po_consumer) ||
							!helper.synchronize(cm_consumer) ||
							!helper.synchronize(pt_consumer) ||
							!helper.synchronize(pf_consumer);
					}

					if(!sync_ko){
						all_panels        = helper.getAllPanels      ();
						all_poseurs       = helper.getAllPoseurs     ();
						all_compains      = helper.getAllCompagnes   ();
						all_panel_types   = helper.getAllPanelTypes  ();
						all_panel_formats = helper.getAllPanelFormats();
					}

					sleep(200);
				} catch (Exception e) {
					sync_ko = true;
					Log.d(getString(R.string.application_name), e.getMessage());
				} finally {
					helper.Close();
				}
				progress_dialog.dismiss();
				finish();
		}}.start();
	}

	public void finish() {
		final Intent data = new Intent();
		sync_ok = false;
		if
		(
			!sync_ko                                                    &&
			(all_panels        != null && !all_panels       .isEmpty()) &&
			(all_poseurs       != null && !all_poseurs      .isEmpty()) &&
			(all_compains      != null && !all_compains     .isEmpty()) &&
			(all_panel_types   != null && !all_panel_types  .isEmpty()) &&
			(all_panel_formats != null && !all_panel_formats.isEmpty())
		){
			sync_ok = true;
		}
		setResult(sync_ok ? RESULT_OK : RESULT_CANCELED, data);
		data.putExtra(Xanadu.SYNC_STATUS, sync_ok);
		super.finish();
	}

	public static List<Panel   > getAllPanels      () { return all_panels       ;}
	public static List<Poseur  > getAllPoseurs     () { return all_poseurs      ;}
	public static List<Campaign> getAllCompains    () { return all_compains     ;}
	public static List<Type    > getAllPanelTypes  () { return all_panel_types  ;}
	public static List<Format  > getAllPanelFormats() { return all_panel_formats;}
}
