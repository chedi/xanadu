package com.xanadu.activities;

import java.util.Timer    ;
import java.util.TimerTask;

import com.xanadu.application.R     ;
import com.xanadu.application.Xanadu;

import android.util.Log                 ;
import android.os.Bundle                ;
import android.app.Activity             ;
import android.content.Intent           ;
import android.content.Context          ;
import android.app.AlertDialog          ;
import android.location.Location        ;
import android.provider.Settings        ;
import android.app.ProgressDialog       ;
import android.content.ComponentName    ;
import android.content.DialogInterface  ;
import android.location.LocationManager ;
import android.location.LocationListener;

public class GpsActivity extends Activity implements LocationListener {
	private long                   time                   ;
	private Context                context                ;
	private Location               location               ;
	private ProgressDialog         progress_dialog        ;
	private LocationManager        location_manager       ;
	private CurrentLocation        current_location       ;
	private AbstractLocationResult location_result_handler;

	public void onCreate(final Bundle savedIS){
		super.onCreate(savedIS);
		setContentView(R.layout.main);

		context          = this;
		location_manager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		current_location = new CurrentLocation(location_manager);

		if (!location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
			buildAlertMessageNoGps();
		} else {
			progress_dialog = ProgressDialog.show(GpsActivity.this, getString(R.string.please_wait), getString(R.string.getting_gps_position), true);
		}

		location_result_handler = new AbstractLocationResult(){
			public void gotLocation(final Location currentLocation){
				if(currentLocation != null){
					time     = System.currentTimeMillis();
					location = currentLocation           ;
					location_manager.removeUpdates(current_location.locationListenerGps    );
					location_manager.removeUpdates(current_location.locationListenerNetwork);
					finish();
		}}};

		try {
			current_location.getLocation(context, location_result_handler);
		} catch (Exception e) {
			Log.d(getString(R.string.application_name), e.getMessage());
		}
		progress_dialog.dismiss();
	}

	public static abstract class AbstractLocationResult{
		public abstract void gotLocation(Location location);
	}

	public void showGpsOptions(){
		final Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		context.startActivity(gpsOptionsIntent);
	}

	private void launchGPSOptions() {
		final Intent        intent   = new Intent       (Settings.ACTION_LOCATION_SOURCE_SETTINGS                      );
		final ComponentName launcher = new ComponentName("com.android.settings","com.android.settings.SecuritySettings");
		intent
			.setFlags    (Intent.FLAG_ACTIVITY_NEW_TASK)
			.addCategory (Intent.CATEGORY_LAUNCHER     )
			.setComponent(launcher                     )
		;
		startActivityForResult(intent, 0);
	}

	public void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.enable_gps_question))
			.setCancelable(false)
			.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int identifier) {
					launchGPSOptions();
		}})
			.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int identifier) {
					dialog.cancel();
		}});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	public static class CurrentLocation{
		private Timer                  timer          ;
		private boolean                gpsEnabled     ;
		private boolean                networkEnabled ;
		private AbstractLocationResult locationResult ;
		private LocationManager        locationManager;

		public CurrentLocation(final LocationManager locationManager) {
			this.gpsEnabled      = false          ;
			this.networkEnabled  = false          ;
			this.locationManager = locationManager;
		}

		LocationListener locationListenerGps = new LocationListener() {
			public void onLocationChanged(final Location location) {
				timer.cancel();
				locationResult.gotLocation(location);
				locationManager.removeUpdates(this);
				locationManager.removeUpdates(locationListenerNetwork);
			}

			public void onProviderEnabled (final String provider) {}
			public void onProviderDisabled(final String provider) {}

			public void onStatusChanged(final String provider, final int status, final Bundle extras) {}
		};

		private LocationListener locationListenerNetwork = new LocationListener() {
			public void onLocationChanged(Location location) {
				timer.cancel();
				locationResult.gotLocation(location);
				locationManager.removeUpdates(this);
				locationManager.removeUpdates(locationListenerGps);
			}
			public void onProviderEnabled (final String provider) {}
			public void onProviderDisabled(final String provider) {}

			public void onStatusChanged(String provider, int status, Bundle extras) {}
		};

		class GetLastLocation extends TimerTask {
			public void run() {
				locationManager.removeUpdates(locationListenerGps    );
				locationManager.removeUpdates(locationListenerNetwork);

				Location gpsLocation     = null;
				Location networkLocation = null;

				if(gpsEnabled    ) { gpsLocation     = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER    );}
				if(networkEnabled) { networkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);}

				if(gpsLocation != null && networkLocation != null){
					if(gpsLocation.getTime() > networkLocation.getTime()) { locationResult.gotLocation(gpsLocation    );}
					else                                                  { locationResult.gotLocation(networkLocation);}
					return;
				}

				if(gpsLocation    !=null) { locationResult.gotLocation(gpsLocation    ); return;}
				if(networkLocation!=null) { locationResult.gotLocation(networkLocation); return;}

				locationResult.gotLocation(null);
		}}

		public boolean getLocation(Context context, AbstractLocationResult result){
			locationResult = result;

			try {
				gpsEnabled     = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER    );
				networkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			} catch(Exception e){
				Log.d(context.getString(R.string.application_name), e.getMessage());
			}

			if(!gpsEnabled && !networkEnabled) {
				return false;
			}

			if(gpsEnabled    ) { locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER    , 0, 0, locationListenerGps    ); }
			if(networkEnabled) { locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork); }

			timer = new Timer();
			timer.schedule(new GetLastLocation(), 50000);
			return true;
		}
	}

	public void onLocationChanged(Location location) {
		if (location != null) {
			this.time      = System.currentTimeMillis();
			this.location  = location;
	}}

	public void onProviderEnabled (String provider) {}
	public void onProviderDisabled(String provider) {}

	public void onStatusChanged(String provider, int status, Bundle extras) {}

	public void finish() {
		final Intent data = new Intent();
		com.xanadu.data.Location data_location = new com.xanadu.data.Location();

		data_location.setLatitude (location.getLatitude ());
		data_location.setLongitude(location.getLongitude());

		data.putExtra(Xanadu.CAPTURE_TIME    , time         );
		data.putExtra(Xanadu.CURRENT_LOCATION, data_location);

		setResult(current_location != null ? RESULT_OK : RESULT_CANCELED, data);
		super.finish();
	}
}
