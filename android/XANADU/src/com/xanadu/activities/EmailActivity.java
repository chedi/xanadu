package com.xanadu.activities;

import com.xanadu.application.R     ;
import com.xanadu.application.Xanadu;

import android.os.Bundle      ;
import android.app.Activity   ;
import android.widget.Toast   ;
import android.content.Intent ;
import android.content.Context;

public class EmailActivity extends Activity {
	public static final String MAIL_FORMAT = "text/plain";
	private Context context;

	public void onCreate(final Bundle saved_instance) {
		super.onCreate(saved_instance);
		context = this ;

		final Bundle   extras           = getIntent().getExtras();
		final String   email_body       = extras.getString(Xanadu.EMAIL_BODY     );
		final String   email_subject    = extras.getString(Xanadu.EMAIL_SUBJECT  );
		final String[] email_recipients = extras.getString(Xanadu.EMAIL_RECIPIENT).split(Xanadu.EMAIL_DOT);

		sendMail(email_body, email_subject, email_recipients);
		finish();
	}

	private void sendMail(final String body, final String subject, final String[] recipients){
		Intent mail_intent = new Intent(Intent.ACTION_SEND);
		mail_intent.setType(MAIL_FORMAT);
		mail_intent.putExtra(Intent.EXTRA_TEXT   , body      );
		mail_intent.putExtra(Intent.EXTRA_EMAIL  , recipients);
		mail_intent.putExtra(Intent.EXTRA_SUBJECT, subject   );

		try {
			startActivity(Intent.createChooser(mail_intent, getString(R.string.send_email)));
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(context, getString(R.string.no_email_client_installed), Toast.LENGTH_SHORT).show();
	}}
}
