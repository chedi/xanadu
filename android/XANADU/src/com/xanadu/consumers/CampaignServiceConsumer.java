package com.xanadu.consumers;

import java.util.List        ;
import java.lang.reflect.Type;

import com.xanadu.data.Campaign;
import com.google.gson.reflect.TypeToken;

public class CampaignServiceConsumer extends AbstractServiceConsumer {
	public static final String ALL_API         = "campaigns/"   ;
	public static final Type   LIST_TYPE_TOKEN = new TypeToken<List<Campaign>>(){}.getType();

	public CampaignServiceConsumer(final String host, final int port, final String login, final String password) {
		super(host, port, login, password);
	}

	public String getAllApi       () { return ALL_API        ;}
	public Type   getListTypeToken() { return LIST_TYPE_TOKEN;}
}