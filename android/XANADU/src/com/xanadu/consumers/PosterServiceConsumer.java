package com.xanadu.consumers;

import java.util.List        ;
import java.lang.reflect.Type;

import com.xanadu.data.Poseur;
import com.google.gson.reflect.TypeToken;

public class PosterServiceConsumer extends AbstractServiceConsumer {
	public static final String ALL_API         = "posters/"     ;
	public static final Type   LIST_TYPE_TOKEN = new TypeToken<List<Poseur>>(){}.getType();

	public PosterServiceConsumer(final String host, final int port, final String login, final String password) {
		super(host, port, login, password);
	}

	public String getAllApi       () { return ALL_API        ;}
	public Type   getListTypeToken() { return LIST_TYPE_TOKEN;}
}