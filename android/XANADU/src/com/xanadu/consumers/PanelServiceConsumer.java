package com.xanadu.consumers;

import java.util.List        ;
import java.lang.reflect.Type;

import com.xanadu.data.Panel;
import com.google.gson.reflect.TypeToken;

public class PanelServiceConsumer extends AbstractServiceConsumer{
	public static final String ALL_API         = "panels/"      ;
	public static final Type   LIST_TYPE_TOKEN = new TypeToken<List<Panel>>(){}.getType();
	
	public PanelServiceConsumer(final String host, final int port, final String login, final String password) {
		super(host, port, login, password);
	}
	
	public String getAllApi       () { return ALL_API        ;}
	public Type   getListTypeToken() { return LIST_TYPE_TOKEN;}
}
