package com.xanadu.utils;

public class CommonUtils {
	public static void killApp(boolean kill_safely) {
		if (kill_safely) {
			System.runFinalizersOnExit(true);
			System.exit(0);
		} else {
			android.os.Process.killProcess(android.os.Process.myPid());
		}
	}
}
