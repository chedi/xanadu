package com.xanadu.utils;

import java.io.FileInputStream               ;
import java.security.MessageDigest           ;
import java.security.NoSuchAlgorithmException;

public class SHAHashing{
	private String salt          ;
	private String plainPassword ;
	private String hashedPassword;

	public static String SHAFileHashing(final String file_path) {
		String result = null;
		try {
			int    read       = 0;
			byte[] data_bytes = new byte[1024];
			final MessageDigest digest        = MessageDigest.getInstance("SHA-1");
			FileInputStream file_input_stream = new FileInputStream(file_path);

			while ((read = file_input_stream.read(data_bytes)) != -1) {
				digest.update(data_bytes, 0, read);
			}
			result = convertToHex(digest.digest());
		} catch (Exception e) {
			e.printStackTrace();
			result = null;
		}
		return result;
	}

	public SHAHashing(final String plainPassword, final String salt) throws NoSuchAlgorithmException{
		this.salt          = salt;
		this.plainPassword = plainPassword;

		final MessageDigest digest = MessageDigest.getInstance("SHA-1");

		digest.update(salt         .getBytes());
		digest.update(plainPassword.getBytes());

		hashedPassword = convertToHex(digest.digest());
	}

	private static String convertToHex(byte[] data){
		final StringBuffer hex_string = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			final String hex=Integer.toHexString(0xff & data[i]);
			if(hex.length()==1){
				hex_string.append('0');
			}
			hex_string.append(hex);
		}
		return hex_string.toString();
	}

	public String getSalt          () { return salt          ;}
	public String getPlainPassword () { return plainPassword ;}
	public String getHashedPassword() { return hashedPassword;}

	public void setSalt         (final String salt         ) { this.salt          = salt         ;}
	public void setPlainPassword(final String plainPassword) { this.plainPassword = plainPassword;}
}