package com.xanadu.utils;

import java.util.concurrent.Callable;

import com.xanadu.application.R;

import android.util.Log               ;
import android.app.AlertDialog        ;
import android.content.Context        ;
import android.content.DialogInterface;

public class SimpleAlertDialog<T> {
	public SimpleAlertDialog(final Context context, final String message, final Callable<T> responseFunc){
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder
			.setMessage(message)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int which) {
					try                 { responseFunc.call() ;}
					catch (Exception e) { Log.d(context.getString(R.string.application_name), e.getMessage());
			}}}).setCancelable(false);
		final AlertDialog alert = builder.create();
		alert.show();
	}

	public SimpleAlertDialog(final Context context, final String message) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder
			.setMessage(message)
			.setCancelable(false)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() { public void onClick(final DialogInterface dialog, final int which) {
				Log.d(context.getString(R.string.application_name), "hiding AlertDialog");
			}});
		final AlertDialog alert = builder.create();
		alert.show();
	}
}
