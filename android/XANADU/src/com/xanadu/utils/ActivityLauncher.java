package com.xanadu.utils;

import java.util.List      ;
import java.io.Serializable;

import android.util.Log       ;
import android.app.Activity   ;
import android.content.Intent ;
import android.content.Context;

import com.xanadu.application.R;

public class ActivityLauncher<T extends Activity, U extends Activity> {
	public class Extras<EXT extends Serializable>{
		private String extraName ;
		private EXT    extraValue;

		public Extras(final String extraName, final EXT extraValue) {
			this.extraName  = extraName ;
			this.extraValue = extraValue;
		}

		public String getExtraName () { return extraName  ;}
		public EXT    getExtraValue() { return extraValue ;}

		public void setExtraName (final String extraName ) { this.extraName = extraName  ;}
		public void setExtraValue(final EXT    extraValue) { this.extraValue = extraValue;}
	}

	private boolean         initOK       ;
	private List<Extras<?>> extras       ;
	private Context         context      ;
	private int             requestCode  ;
	private Class<T>        activityClass;

	private void init(final Class<T> activityClass, final int requestCode, final Context context){
		try {
			if(context instanceof Activity){
				initOK             = true         ;
				this.context       = context      ;
				this.requestCode   = requestCode  ;
				this.activityClass = activityClass;
			}else{
				initOK = false;
			}
		}catch(Exception e){
			Log.d(context.getString(R.string.application_name), e.getMessage());
	}}

	public ActivityLauncher(final Class<T> activityClass, final int requestCode, final Context context) {
		init(activityClass, requestCode, context);
	}

	public ActivityLauncher(final Class<T> activityClass, final int requestCode, final Context context, final List<Extras<?>> extras) {
		this.extras = extras;
		init(activityClass, requestCode, context);
	}

	public boolean run(){
		if(initOK){
			final Intent intent = new Intent(context, activityClass);
			if(extras != null){
				for(int i=0;i < extras.size();i++){
					intent.putExtra(extras.get(i).extraName, extras.get(i).extraValue);
			}}
			((Activity) context).startActivityForResult(intent, requestCode);
		}
		return initOK;
	}

	public List<Extras<?>> getExtras       () { return extras       ;}
	public boolean         isInitOK        () { return initOK       ;}
	public Context         getContext      () { return context      ;}
	public int             getRequestCode  () { return requestCode  ;}
	public Class<T>        getActivityClass() { return activityClass;}

	public void setExtras       (final List<Extras<?>> extras       ) { this.extras        = extras       ;}
	public void setInitOK       (final boolean         initOK       ) { this.initOK        = initOK       ;}
	public void setContext      (final Context         context      ) { this.context       = context      ;}
	public void setRequestCode  (final int             requestCode  ) { this.requestCode   = requestCode  ;}
	public void setActivityClass(final Class<T>        activityClass) { this.activityClass = activityClass;}
}
