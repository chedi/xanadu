package com.xanadu.data;

import com.xanadu.data.meta.TablePK      ;
import com.xanadu.data.meta.TableGet     ;
import com.xanadu.data.meta.TableSet     ;
import com.xanadu.data.meta.TableMeta    ;
import com.xanadu.data.meta.AbstractTable;

@TableMeta(tableName = "SimInfos")
public class SimInfo extends AbstractTable<SimInfo> {
	private static final long serialVersionUID = -1529858172230926260L;

	private long   id          ;
	private String deviceId    ;
	private String serialNumber;

	public SimInfo(final long id, final String deviceId, final String phoneNumber, final String simSerialNumber) {
		super();
		this.id           = id             ;
		this.deviceId     = deviceId       ;
		this.serialNumber = simSerialNumber;
	}

	public SimInfo       () { super();     }
	public SimInfo thiz  () { return this ;}

	@TablePK
	@TableGet( getName = "id"             )public long   getId          () { return id          ;}
	@TableGet( getName = "deviceId"       )public String getDeviceId    () { return deviceId    ;}
	@TableGet( getName = "simSerialNumber")public String getSerialNumber() { return serialNumber;}

	@TableSet( getName = "id"             )public void setId          (final long   id             ) { this.id           = id             ;}
	@TableSet( getName = "deviceId"       )public void setDeviceId    (final String deviceId       ) { this.deviceId     = deviceId       ;}
	@TableSet( getName = "simSerialNumber")public void setSerialNumber(final String simSerialNumber) { this.serialNumber = simSerialNumber;}
}