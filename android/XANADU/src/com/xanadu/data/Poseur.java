package com.xanadu.data;

import com.xanadu.data.meta.TablePK      ;
import com.xanadu.data.meta.TableGet     ;
import com.xanadu.data.meta.TableSet     ;
import com.xanadu.data.meta.TableMeta    ;
import com.xanadu.data.meta.AbstractTable;

@TableMeta(tableName = "Poseurs")
public class Poseur extends AbstractTable<Poseur> {
	private static final long serialVersionUID = 6896996513869243412L;

	private long   id        ;
	private User   user      ;
	private String telephones;

	public Poseur       (                                                       ) { super();                           }
	public Poseur       (final long id, final User user, final String telephones) { super();init(id, user, telephones);}
	public Poseur thiz  (                                                       ) { return this                       ;}

	private void init(final long id, final User user, final String telephones){
		this.id         = id        ;
		this.user       = user      ;
		this.telephones = telephones;
	}

	@TablePK
	@TableGet(getName = "id"        )public long   getId        () { return id        ;}
	@TableGet(getName = "user"      )public User   getUser      () { return user      ;}
	@TableGet(getName = "telephones")public String getTelephones() { return telephones;}

	@TableSet(getName = "id"        )public void setId        (final long   id        ) { this.id         = id        ;}
	@TableSet(getName = "user"      )public void setUser      (final User   user      ) { this.user       = user      ;}
	@TableSet(getName = "telephones")public void setTelephones(final String telephones) { this.telephones = telephones;}
}