package com.xanadu.data;

import com.xanadu.data.meta.TablePK      ;
import com.xanadu.data.meta.TableGet     ;
import com.xanadu.data.meta.TableSet     ;
import com.xanadu.data.meta.TableMeta    ;
import com.xanadu.data.meta.AbstractTable;

@TableMeta(tableName = "Campaigns")
public class Campaign extends AbstractTable<Campaign> {
	private static final long serialVersionUID = -8686664228342574409L;

	private long   id       ;
	private String label    ;
	private String endDate  ;
	private String startDate;

	public Campaign(final long id, final String label, final String endDate, final String startDate) {
		super();
		this.id        = id       ;
		this.label     = label    ;
		this.endDate   = endDate  ;
		this.startDate = startDate;
	}

	public Campaign          () { super()      ;}
	public Campaign thiz     () { return this  ;}
	public String   toString () { return label ;}

	@TablePK
	@TableGet(getName = "id"       )public long   getId       () { return id       ;}
	@TableGet(getName = "label"    )public String getLabel    () { return label    ;}
	@TableGet(getName = "endDate"  )public String getEndDate  () { return endDate  ;}
	@TableGet(getName = "startDate")public String getStartDate() { return startDate;}

	@TableSet(getName = "id"       )public void setId       (final long   id       ) { this.id        = id       ;}
	@TableSet(getName = "label"    )public void setLabel    (final String label    ) { this.label     = label    ;}
	@TableSet(getName = "endDate"  )public void setEndDate  (final String endDate  ) { this.endDate   = endDate  ;}
	@TableSet(getName = "startDate")public void setStartDate(final String startDate) { this.startDate = startDate;}
}