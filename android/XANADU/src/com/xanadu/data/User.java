package com.xanadu.data;

import com.xanadu.data.meta.TablePK      ;
import com.xanadu.data.meta.TableGet     ;
import com.xanadu.data.meta.TableSet     ;
import com.xanadu.data.meta.TableMeta    ;
import com.xanadu.data.meta.AbstractTable;

@TableMeta(tableName = "Users")
public class User extends AbstractTable<User> {
	private static final long serialVersionUID = 6176461128080557241L;

	private long   id      ;
	private String username;
	private String password;

	public User(final long id, final String login, final String password) {
		super();
		this.id       = id      ;
		this.username = login   ;
		this.password = password;
	}

	public User      (){ super();     }
	public User thiz (){ return this; }

	@TablePK
	@TableGet( getName = "id"      ) public long   getId      () { return id      ;}
	@TableGet( getName = "username") public String getUsername() { return username;}
	@TableGet( getName = "password") public String getPassword() { return password;}

	@TableSet( getName = "id"      ) public void setId      (final long   id      ) { this.id       = id      ;}
	@TableSet( getName = "username") public void setUsername(final String login   ) { this.username = login   ;}
	@TableSet( getName = "password") public void setPassword(final String password) { this.password = password;}
}