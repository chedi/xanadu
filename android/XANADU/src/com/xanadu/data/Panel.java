package com.xanadu.data;

import com.xanadu.data.meta.TablePK      ;
import com.xanadu.data.meta.TableGet     ;
import com.xanadu.data.meta.TableSet     ;
import com.xanadu.data.meta.TableMeta    ;
import com.xanadu.data.meta.AbstractTable;

@TableMeta(tableName = "Panels")
public class Panel extends AbstractTable<Panel> {
	private static final long serialVersionUID = -6834988778562006234L;

	private long     id      ;
	private String   code    ;
	private Type     type    ;
	private Format   format  ;
	private Location location;

	public Panel(final long id, final String code, final Type type, final Format format, final Location location) {
		super();
		this.id       = id      ;
		this.code     = code    ;
		this.type     = type    ;
		this.format   = format  ;
		this.location = location;
	}

	public Panel          () { super();     }
	public Panel  thiz    () { return this ;}
	public String toString() { return code ;}

	@TablePK
	@TableGet(getName = "id"      )public long        getId      () { return id      ;}
	@TableGet(getName = "code"    )public String      getCode    () { return code    ;}
	@TableGet(getName = "type"    )public Type        getType    () { return type    ;}
	@TableGet(getName = "format"  )public Format      getFormat  () { return format  ;}
	@TableGet(getName = "location")public Location    getLocation() { return location;}

	@TableSet(getName = "id"      )public void setId      (final long     id      ) { this.id       = id      ;}
	@TableSet(getName = "code"    )public void setCode    (final String   code    ) { this.code     = code    ;}
	@TableSet(getName = "type"    )public void setType    (final Type     type    ) { this.type     = type    ;}
	@TableSet(getName = "format"  )public void setFormat  (final Format   format  ) { this.format   = format  ;}
	@TableSet(getName = "location")public void setLocation(final Location location) { this.location = location;}
}