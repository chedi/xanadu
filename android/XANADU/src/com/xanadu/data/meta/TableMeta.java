package com.xanadu.data.meta;

import java.lang.annotation.Target         ;
import java.lang.annotation.Retention      ;
import java.lang.annotation.ElementType    ;
import java.lang.annotation.RetentionPolicy;

@Target   (ElementType    .TYPE   )
@Retention(RetentionPolicy.RUNTIME)
public @interface TableMeta {
	public String tableName();
}

