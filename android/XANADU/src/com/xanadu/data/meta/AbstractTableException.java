package com.xanadu.data.meta;

public class AbstractTableException extends Exception{
	private static final long serialVersionUID = -5671860263361163390L;
	
	public AbstractTableException(final String message){
		super(message);
	}
}
