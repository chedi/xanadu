/*
 * Campaigns
 */
Ext.define('AllTrainingFields', {
	extend: 'Ext.data.Model',
	fields: ['id', 'label', 'description'],
	proxy: 
	{
		url   : '/training/api/trainingFields/',
		type  : 'ajax',
		reader: 'json'
	},
	belongsTo : 'Training'
});

Ext.define('UserTrainingFields', {
	extend: 'Ext.data.Model',
	fields: ['id', 'label', 'description'],
	proxy: 
	{
		url   : '/training/api/trainingFields/'+ user_id + '/',
		type  : 'ajax',
		reader: 'json'
	},
	belongsTo : 'Training'
});

var all_training_fields  = Ext.create('Ext.data.Store', { model: 'AllTrainingFields' , autoLoad: true });
var user_training_fields = Ext.create('Ext.data.Store', { model: 'UserTrainingFields', autoLoad: true });