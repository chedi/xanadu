Ext.define('CampaignViewer.CampaignPanel', {
	extend       : 'Ext.panel.Panel',
	alias        : 'widget.CampaignPanel',
	title        : 'Campaings',
	layout       : 'fit',
	animCollapse : true,

	initComponent: function(){
		Ext.apply(this, {
			items      : this.createView(),
			dockedItems: this.createToolbar()
		});
		this.createMenu();
		this.addEvents('campaignSelect');
		this.callParent(arguments);
	},

	createView: function(){
		this.view = Ext.create('widget.dataview', {
			store: campaigns,
			selModel: {
				mode: 'SINGLE',
				listeners: {
					scope: this,
					selectionchange: this.onSelectionChange
				}
			},
			listeners: {
				scope: this,
				viewready: this.onViewReady,
				contextmenu: this.onContextMenu,
			},
			trackOver: true,
			cls: 'campaign-list',
			itemSelector: '.campaign-list-item',
			overItemCls: 'campaign-list-item-hover',
			tpl: '<tpl for="."><div class="campaign-list-item">{label}</div></tpl>'
		});
		return this.view;
	},

	onViewReady: function(){
		this.view.getSelectionModel().select(this.view.store.first());
	},

	createToolbar: function(){
		this.createActions();
		this.toolbar = Ext.create('widget.toolbar', {
			items: [
				this.filterAction,
				this.refreshAction,
			]
		});
		return this.toolbar;
	},

	createActions: function(){
		this.filterAction = Ext.create('Ext.Action', {
			scope  : this,
			text   : 'filter',
			iconCls: 'campaign-filter',
			handler: this.onFilterClick,
		});

		this.refreshAction = Ext.create('Ext.Action', {
			itemId : 'refresh',
			scope  : this,
			handler: this.onRefreshClick,
			text   : 'refesh',
			iconCls: 'campaign-refresh'
		});
	},

	createMenu: function(){
		this.menu = Ext.create('widget.menu', {
			items: [{
				scope  : this,
				handler: this.onLoadClick,
				text   : 'Load campaigns',
				iconCls: 'campaign-load'
			}, this.removeAction, '-', this.addAction],
			listeners: {
				hide: function(c){
					c.activeRequest = null;
		}}});
	},

	onSelectionChange: function(){
		var selected = this.getSelectedItem();
		this.loadCampaign(selected);
	},

	onLoadClick: function(){
		this.loadCampaign(this.menu.activeRequest);
	},

	loadCampaign: function(rec){
		if (rec) {
			this.fireEvent('campaignSelect', this, rec.get('id'), rec.get('label'));
		}
	},

	getSelectedItem: function(){
		return this.view.getSelectionModel().getSelection()[0] || false;
	},

	onContextMenu: function(view, index, el, event){
		var menu = this.menu;
		event.stopEvent();
		menu.activeRequest = view.store.getAt(index);
		menu.showAt(event.getXY());
	},

	onFilterClick: function(){
		var win = Ext.create('widget.CampaingFiltersWindow', {
			listeners: {
				scope     : this,
				filtersSet: this.onFiltersSet
			}
		});
		win.show();
	},

	onRefreshClick : function(){
		this.fireEvent('trainerremove', this, active.get('title'));
	}

	onFiltersSet: function(win){
		this.animateNode(view.getNode(rec), 0, 1);
	},

	animateNode: function(el, start, end, listeners){
		Ext.create('Ext.fx.Anim', {
			target: Ext.get(el),
			duration: 500,
			from: { opacity: start},
			to  : { opacity: end  },
			listeners: listeners
		 });
	},

	onDestroy: function(){
		this.callParent(arguments);
		this.menu.destroy();
	}
});
