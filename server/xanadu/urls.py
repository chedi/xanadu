from django.contrib import admin
from django.conf.urls.defaults import patterns, include, url

admin.autodiscover()

urlpatterns = patterns(''  ,
	(r'^$'                 , 'xanadu.management.views.profile'),
	(r'^admin/'            , include(admin.site.urls)),
	(r'^admin_tools/'      , include('admin_tools.urls')),
	(r'^login/?$'          , 'django.contrib.auth.views.login', {'template_name' : 'login.html'}),
	(r'^main/'             , 'xanadu.management.views.main'),
	(r'^accounts/profile/$', 'xanadu.management.views.profile'),
	(r'^api/'              , include('xanadu.api.urls')),
	(r'^photologue/'       , include('photologue.urls')),
)
