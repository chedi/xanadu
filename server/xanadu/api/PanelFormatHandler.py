from piston.handler           import BaseHandler
from xanadu.management.models import *

class PanelFormatHandler(BaseHandler):
	model           = PanelFormat
	allowed_methods = ('GET',)
	fields = ('id', 'label')

	def read(self, request, panel_format_id=None):
		base = PanelFormat.objects
		if panel_format_id:
			return base.filter(id=panel_format_id)
		else:
			return base.all()