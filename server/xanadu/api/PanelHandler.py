from piston.handler           import BaseHandler
from xanadu.management.models import *

class PanelHandler(BaseHandler):
	model           = Panel
	allowed_methods = ('GET',)
	fields = ('id','code', ('location',('id', 'longitude', 'latitude')), 'type', 'format')

	def read(self, request, id=None, type=None, format=None):
		data = None;
		base = Panel.objects
		if id:
			data = base.filter(id=id)
		elif type and format:
			data = base.filter(type=type).filter(format=format)
		else:
			data = base.all()
		return data
