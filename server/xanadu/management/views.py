import logging

from django.conf               import settings
from django.http               import HttpResponse
from django.http               import HttpResponseRedirect
from django.template           import RequestContext
from django.shortcuts          import render_to_response
from django.contrib.auth.views import redirect_to_login

from django.contrib.auth      import logout
from xanadu.management.models import *

from django.db                      import transaction
from django.utils                   import simplejson
from vectorformats.Formats          import Django
from vectorformats.Formats          import GeoJSON
from django.views.decorators.csrf   import csrf_protect
from django.views.decorators.cache  import never_cache
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

from django.core import serializers


from chartit               import DataPool, Chart
from photologue.models     import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

logger = logging.getLogger(__name__)

@csrf_protect
@login_required(login_url='/xanadu/login/')
def profile(request):
	return render_to_response('main.html', {}, context_instance=RequestContext(request))

@csrf_protect
@login_required(login_url='/xanadu/login/')
def main(request):
	return render_to_response('main.html', {}, context_instance=RequestContext(request))