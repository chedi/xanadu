from django.db                  import models
from django.conf                import settings
from photologue.models          import Photo
from photologue.models          import Gallery
from django.contrib.gis.db      import models
from django.core.exceptions     import ValidationError
from django.contrib.auth.models import User

class PanelType(models.Model):
	label = models.CharField( max_length = 50, unique = True)
	def __unicode__(self):
		return self.label

class PanelFormat(models.Model):
	label = models.CharField( max_length = 50, unique = True)
	def __unicode__(self):
		return self.label

class Panel(models.Model):
	code     = models.CharField    ( max_length = 30, unique = True )
	type     = models.ForeignKey   ( PanelType                      )
	format   = models.ForeignKey   ( PanelFormat                    )
	position = models.PointField   ( srid = 3857                    )
	verified = models.BooleanField ( default = False                )
	def __unicode__(self):
		return self.code

class Device(models.Model):
	user   = models.ForeignKey ( User, unique = True            )
	serial = models.CharField  ( max_length = 30, unique = True )
	label  = models.CharField  ( max_length = 20, unique = True )
	def __unicode__(self):
		return u"%s" % (self.label)

class Poster(models.Model):
	user       = models.ForeignKey ( User, unique = True           )
	telephones = models.CharField  ( max_length = 20, unique = True)
	def __unicode__(self):
		return u"%s" % (self.user.username)

class Client(models.Model):
	raison_sociale = models.TextField (                     )
	user           = models.ForeignKey( User, unique = True )
	def __unicode__(self):
		return self.raison_sociale

class Campaign(models.Model):
	label     = models.TextField    (        )
	client    = models.ForeignKey   ( Client )
	publie    = models.BooleanField (        )
	endDate   = models.DateField    (        )
	startDate = models.DateField    (        )
	def __unicode__(self):
		return self.label
	def clean(self):
		if self.startDate > self.endDate:
			raise ValidationError('end date must not precede start date')

class Event(models.Model):
	time       = models.DateField  (                                )
	image      = models.ForeignKey ( Photo                          )
	panel      = models.ForeignKey ( Panel                          )
	poster     = models.ForeignKey ( Poster                         )
	objects    = models.GeoManager (                                )
	position   = models.PointField ( srid = 3857                    )
	campaign   = models.ForeignKey ( Campaign                       )
	image_hash = models.CharField  ( max_length = 256, unique = True)
	def __unicode__(self):
		return u"(%s, %s)[%s](%s : %s)" % (self.position.x, self.position.y, self.campaign, self.time, self.poster)
	def coordinates(self):
		return [self.position.x, self.position.y]
	def image_container(self):
		return self.image.admin_thumbnail()
	image_container.allow_tags = True