import os
import random
import sndhdr

from django.forms             import *
from xanadu.management.models import *
from django.contrib           import admin
from django.contrib.gis.admin import OSMGeoAdmin

from PIL                          import Image
from django.conf                  import settings
from django.contrib.gis.db        import models
from django.utils.safestring      import mark_safe
from django.utils.translation     import ugettext as _
from django.contrib.admin.widgets import AdminFileWidget

def get_hexdigest(algorithm, salt, raw_password):
    """
    Returns a string of the hexdigest of the given plaintext password and salt
    using the given algorithm ('md5', 'sha1' or 'crypt').
    """
    raw_password, salt = smart_str(raw_password), smart_str(salt)
    if algorithm == 'crypt':
        try:
            import crypt
        except ImportError:
            raise ValueError('"crypt" password algorithm not supported in this environment')
        return crypt.crypt(raw_password, salt)

    if algorithm == 'md5':
        return md5_constructor(salt + raw_password).hexdigest()
    elif algorithm == 'sha1':
        return sha_constructor(salt + raw_password).hexdigest()
    raise ValueError("Got unknown password algorithm type in password.")

class PanelAdmin(OSMGeoAdmin):
	list_display        = ('code', 'position', 'type', 'format', 'verified')
	list_editable       = ('position', 'type', 'format')
	search_fields       = ('code', 'type', 'format', 'verified')
	list_per_page       = 10
	ordering            = ('code',)
	save_as             = True
	list_select_related = True
	fieldsets = (
		('Informations' ,        {'fields': ['code', 'type', 'format',], 'classes': ('show','extrapretty')}),
		('Position Geomertique', {'fields': ['position'              ,], 'classes': ('show', 'wide'      )}),
	)
	scrollable = False
	map_width  = 400
	map_height = 325

class LocationAdmin(OSMGeoAdmin):
	list_display        = ('position',)
	list_per_page       = 10
	ordering            = ('position',)
	save_as             = True
	list_select_related = True
	fieldsets = (
	  ('Position Geomertique', {'fields': ['position',], 'classes': ('show', 'wide')}),
	)
	scrollable = False
	map_width  = 400
	map_height = 325

class EventAdmin(OSMGeoAdmin):
	list_display        = ('poster', 'campaign', 'time', 'position', 'image_container')
	list_editable       = ('campaign','position'          ,)
	search_fields       = ('poster', 'campaign')
	list_per_page       = 10
	ordering            = ('time', 'campaign', 'poster')
	save_as             = True
	list_select_related = True
	fieldsets           = (
		('Informations',        {'fields': ['time', 'poster', 'campaign','panel',], 'classes' : ('show','extrapretty')}),
		('Image',               {'fields': ['image_hash', 'image'               ,], 'classes' : ('show','wide'       )}),
		('Position Geomertique',{'fields': ['position'                          ,], 'classes' : ('show','wide'       )}),
	)
	scrollable = False
	map_width  = 400
	map_height = 325
